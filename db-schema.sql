CREATE TYPE "task_type" AS ENUM (
  'homework',
  'assignment',
  'test'
);

CREATE TYPE "day" AS ENUM (
  'Monday1',
  'Tuesday1',
  'Wednesday1',
  'Thursday1',
  'Friday1',
  'Monday2',
  'Tuesday2',
  'Wednesday2',
  'Thursday2',
  'Friday2'
);

CREATE TABLE "class" (
  "id" SERIAL UNIQUE PRIMARY KEY NOT NULL,
  "name" varchar UNIQUE NOT NULL,
  "teacher" id NOT NULL
);

CREATE TABLE "students_class" (
  "id" SERIAL UNIQUE PRIMARY KEY NOT NULL,
  "class_id" int NOT NULL,
  "student_id" id NOT NULL
);

CREATE TABLE "task" (
  "id" SERIAL UNIQUE PRIMARY KEY NOT NULL,
  "class_id" int NOT NULL,
  "name" varchar NOT NULL,
  "type" task_type NOT NULL,
  "description" varchar,
  "total_marks" int,
  "posted" datetime NOT NULL,
  "due_date" datetime NOT NULL
);

CREATE TABLE "marks" (
  "id" SERIAL UNIQUE PRIMARY KEY NOT NULL,
  "task_id" int NOT NULL,
  "student_id" int NOT NULL,
  "mark" int NOT NULL,
  "percentage" real NOT NULL
);

CREATE TABLE "timetable" (
  "id" SERIAL UNIQUE PRIMARY KEY NOT NULL,
  "class_id" int NOT NULL,
  "start_time" timestamp NOT NULL,
  "end_time" timestamp NOT NULL,
  "day" day NOT NULL
);

CREATE TABLE "term" (
  "id" SERIAL UNIQUE PRIMARY KEY NOT NULL,
  "start_date" date UNIQUE NOT NULL,
  "end_date" date UNIQUE NOT NULL
);

CREATE TABLE "behaviour_notes" (
  "id" SERIAL UNIQUE PRIMARY KEY NOT NULL,
  "students_id" int NOT NULL,
  "note" text NOT NULL
);

ALTER TABLE "students_class" ADD FOREIGN KEY ("class_id") REFERENCES "class" ("id");

ALTER TABLE "task" ADD FOREIGN KEY ("class_id") REFERENCES "class" ("id");

ALTER TABLE "marks" ADD FOREIGN KEY ("task_id") REFERENCES "task" ("id");

ALTER TABLE "marks" ADD FOREIGN KEY ("student_id") REFERENCES "students_class" ("id");

ALTER TABLE "timetable" ADD FOREIGN KEY ("class_id") REFERENCES "class" ("id");

ALTER TABLE "behaviour_notes" ADD FOREIGN KEY ("students_id") REFERENCES "students_class" ("id");
